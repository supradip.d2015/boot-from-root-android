package p002eu.chainfire.librootjava;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/* renamed from: eu.chainfire.librootjava.a */
public class C0075a {

    /* renamed from: a */
    public static final String f244a = (VERSION.SDK_INT < 23 ? "toolbox" : "toybox");

    /* renamed from: b */
    public static final String f245b = m277e();

    /* renamed from: a */
    public static String m269a() {
        String[] strArr;
        for (String str : new String[]{"/system/bin/app_process_original", "/system/bin/app_process_init", "/system/bin/app_process"}) {
            if (new File(str).exists()) {
                return str;
            }
        }
        return null;
    }

    /* renamed from: a */
    public static String m270a(Context context, String str, List<String> list, List<String> list2, String str2) {
        StringBuilder sb;
        String str3;
        if (str == null) {
            str = m275c();
        }
        if (str2 == null) {
            if (!m276d()) {
                return str;
            }
            str2 = "/dev";
            if ((context.getApplicationInfo().flags & 262144) == 0) {
                File cacheDir = context.getCacheDir();
                try {
                    cacheDir.mkdirs();
                } catch (Exception unused) {
                }
                if (cacheDir.exists()) {
                    try {
                        str2 = cacheDir.getCanonicalPath();
                    } catch (IOException unused2) {
                    }
                }
            }
        }
        boolean startsWith = str2.startsWith("/data/");
        if (m272a(str)) {
            sb = new StringBuilder();
            sb.append(str2);
            str3 = "/.app_process64_";
        } else {
            sb = new StringBuilder();
            sb.append(str2);
            str3 = "/.app_process32_";
        }
        sb.append(str3);
        sb.append(f245b);
        String sb2 = sb.toString();
        list.add(String.format(Locale.ENGLISH, "%s cp %s %s >/dev/null 2>/dev/null", new Object[]{f244a, str, sb2}));
        Locale locale = Locale.ENGLISH;
        Object[] objArr = new Object[3];
        objArr[0] = f244a;
        objArr[1] = startsWith ? "0766" : "0700";
        objArr[2] = sb2;
        list.add(String.format(locale, "%s chmod %s %s >/dev/null 2>/dev/null", objArr));
        if (startsWith) {
            list.add(String.format(Locale.ENGLISH, "restorecon %s >/dev/null 2>/dev/null", new Object[]{sb2}));
        }
        list2.add(String.format(Locale.ENGLISH, "%s rm %s >/dev/null 2>/dev/null", new Object[]{f244a, sb2}));
        return sb2;
    }

    /* renamed from: a */
    public static String m271a(boolean z) {
        String[] strArr;
        for (String str : new String[]{"/system/bin/app_process32_original", "/system/bin/app_process32_init", "/system/bin/app_process32"}) {
            if (new File(str).exists()) {
                return str;
            }
        }
        if (z) {
            return m269a();
        }
        return null;
    }

    /* renamed from: a */
    public static boolean m272a(String str) {
        if (!m278f()) {
            return false;
        }
        int lastIndexOf = str.lastIndexOf(47);
        String substring = lastIndexOf >= 0 ? str.substring(lastIndexOf + 1) : str;
        if (substring.contains("32")) {
            return false;
        }
        if (substring.contains("64")) {
            return true;
        }
        try {
            String name = new File(str).getCanonicalFile().getName();
            if (name.contains("32")) {
                return false;
            }
            if (name.contains("64")) {
                return true;
            }
            Boolean b = m273b(str);
            return b != null ? b.booleanValue() : !m279g();
        } catch (Exception e) {
            C0078d.m287a(e);
        }
    }

    /* renamed from: b */
    private static Boolean m273b(String str) {
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream(str);
            fileInputStream.skip(4);
            int read = fileInputStream.read();
            if (read == 1) {
                Boolean valueOf = Boolean.valueOf(false);
                fileInputStream.close();
                return valueOf;
            } else if (read == 2) {
                Boolean valueOf2 = Boolean.valueOf(true);
                fileInputStream.close();
                return valueOf2;
            } else {
                fileInputStream.close();
                return null;
            }
        } catch (FileNotFoundException unused) {
        } catch (Exception e) {
            C0078d.m287a(e);
        } catch (Throwable th) {
            fileInputStream.close();
            throw th;
        }
    }

    /* renamed from: b */
    public static String m274b() {
        String[] strArr;
        for (String str : new String[]{"/system/bin/app_process64_original", "/system/bin/app_process64_init", "/system/bin/app_process64"}) {
            if (new File(str).exists()) {
                return str;
            }
        }
        return null;
    }

    /* renamed from: c */
    public static String m275c() {
        String b = !m279g() ? m274b() : null;
        return b == null ? m271a(true) : b;
    }

    @TargetApi(23)
    /* renamed from: d */
    public static boolean m276d() {
        return VERSION.SDK_INT < 29 && (VERSION.SDK_INT != 28 || VERSION.PREVIEW_SDK_INT == 0);
    }

    /* renamed from: e */
    private static String m277e() {
        String str = null;
        while (true) {
            if (str != null && !str.contains("32") && !str.contains("64")) {
                return str;
            }
            str = UUID.randomUUID().toString();
        }
    }

    @SuppressLint({"ObsoleteSdkInt"})
    @TargetApi(21)
    /* renamed from: f */
    private static boolean m278f() {
        boolean z = false;
        if (VERSION.SDK_INT < 21) {
            return false;
        }
        if (Build.SUPPORTED_64_BIT_ABIS.length > 0) {
            z = true;
        }
        return z;
    }

    /* renamed from: g */
    private static boolean m279g() {
        if (!m278f()) {
            return false;
        }
        try {
            if (new File("/proc/self/exe").getCanonicalPath().contains("32")) {
                return true;
            }
        } catch (Exception e) {
            C0078d.m287a(e);
        }
        return false;
    }
}
