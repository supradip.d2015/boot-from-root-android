package p002eu.chainfire.librootjava;

import android.util.Log;
import java.util.Locale;

/* renamed from: eu.chainfire.librootjava.d */
public class C0078d {

    /* renamed from: a */
    private static String f248a = m286a();

    /* renamed from: b */
    private static boolean f249b = false;

    /* renamed from: a */
    private static String m286a() {
        String str = "eu.chainfire.librootjava";
        while (true) {
            int indexOf = str.indexOf(46);
            if (indexOf < 0) {
                return str;
            }
            str = str.substring(indexOf + 1);
        }
    }

    /* renamed from: a */
    public static void m287a(Exception exc) {
        if (f249b) {
            m289a("EXCEPTION", "%s: %s", exc.getClass().getSimpleName(), exc.getMessage());
            exc.printStackTrace();
        }
    }

    /* renamed from: a */
    public static void m288a(String str) {
        f248a = str;
    }

    /* renamed from: a */
    public static void m289a(String str, String str2, Object... objArr) {
        if (f249b) {
            if (objArr != null && objArr.length > 0) {
                str2 = String.format(Locale.ENGLISH, str2, objArr);
            }
            String str3 = f248a;
            Locale locale = Locale.ENGLISH;
            Object[] objArr2 = new Object[3];
            objArr2[0] = str;
            objArr2[1] = (str2.startsWith("[") || str2.startsWith(" ")) ? "" : " ";
            objArr2[2] = str2;
            Log.d(str3, String.format(locale, "[%s]%s%s", objArr2));
        }
    }

    /* renamed from: a */
    public static void m290a(String str, Object... objArr) {
        if (f249b) {
            if (objArr != null && objArr.length > 0) {
                str = String.format(Locale.ENGLISH, str, objArr);
            }
            Log.d(f248a, str);
        }
    }

    /* renamed from: a */
    public static void m291a(boolean z) {
        f249b = z;
    }

    /* renamed from: b */
    public static void m292b(String str, Object... objArr) {
        Log.e(f248a, String.format(Locale.ENGLISH, str, objArr));
    }
}
