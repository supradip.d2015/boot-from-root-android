package p002eu.chainfire.librootjava;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build.VERSION;
import dalvik.system.BaseDexClassLoader;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* renamed from: eu.chainfire.librootjava.h */
public class C0083h {

    /* renamed from: a */
    public static final String[] f268a = {".app_process32_", ".app_process64_"};

    /* renamed from: a */
    public static String m313a(Context context, Class<?> cls, String str, String[] strArr, String str2) {
        if (str == null) {
            str = C0075a.m275c();
        }
        String str3 = str;
        return m315a(context.getPackageCodePath(), cls.getName(), str3, C0075a.m272a(str3), strArr, str2);
    }

    @SuppressLint({"SdCardPath"})
    /* renamed from: a */
    public static String m314a(Context context, String str) {
        String[] strArr;
        String[] strArr2;
        if (VERSION.SDK_INT < 23 || (context.getApplicationInfo().flags & 268435456) != 0) {
            if (str.toLowerCase().startsWith("lib")) {
                str = str.substring(3);
            }
            if (str.toLowerCase().endsWith(".so")) {
                str = str.substring(0, str.length() - 3);
            }
            String packageName = context.getPackageName();
            ApplicationInfo applicationInfo = context.getApplicationInfo();
            StringBuilder sb = new StringBuilder();
            sb.append(applicationInfo.nativeLibraryDir);
            sb.append(File.separator);
            sb.append("lib");
            sb.append(str);
            sb.append(".so");
            StringBuilder sb2 = new StringBuilder();
            sb2.append(applicationInfo.nativeLibraryDir);
            sb2.append(File.separator);
            sb2.append(str);
            sb2.append(".so");
            for (String str2 : new String[]{sb.toString(), sb2.toString()}) {
                if (new File(str2).exists()) {
                    return str2;
                }
            }
            if (context.getClassLoader() instanceof BaseDexClassLoader) {
                try {
                    return ((BaseDexClassLoader) context.getClassLoader()).findLibrary(str);
                } catch (Throwable unused) {
                }
            }
            for (String str3 : new String[]{String.format(Locale.ENGLISH, "/data/data/%s/lib/lib%s.so", new Object[]{packageName, str}), String.format(Locale.ENGLISH, "/data/data/%s/lib/%s.so", new Object[]{packageName, str})}) {
                if (new File(str3).exists()) {
                    return str3;
                }
            }
            return null;
        }
        throw new RuntimeException("librootjava: incompatible with extractNativeLibs=\"false\" in your manifest");
    }

    /* renamed from: a */
    public static String m315a(String str, String str2, String str3, boolean z, String[] strArr, String str4) {
        StringBuilder sb;
        String str5;
        String str6 = System.getenv("ANDROID_ROOT");
        StringBuilder sb2 = new StringBuilder();
        if (str6 != null) {
            sb2.append("ANDROID_ROOT=");
            sb2.append(str6);
            sb2.append(' ');
        }
        String[] strArr2 = null;
        int lastIndexOf = str3.lastIndexOf(47);
        if (lastIndexOf >= 0) {
            strArr2 = new String[]{str3.substring(0, lastIndexOf)};
        }
        String a = C0077c.m281a(z, strArr2);
        if (a != null) {
            sb2.append("LD_LIBRARY_PATH=");
            sb2.append(a);
            sb2.append(' ');
        }
        String str7 = "";
        String str8 = "";
        if (str4 != null) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str8);
            sb3.append(" --nice-name=");
            sb3.append(str4);
            str8 = sb3.toString();
        }
        if (C0076b.f246a) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append(str7);
            sb4.append(" -Xcompiler-option --debuggable");
            String sb5 = sb4.toString();
            if (VERSION.SDK_INT >= 28) {
                sb = new StringBuilder();
                sb.append(sb5);
                str5 = " -XjdwpProvider:internal -XjdwpOptions:transport=dt_android_adb,suspend=n,server=y";
            } else {
                sb = new StringBuilder();
                sb.append(sb5);
                str5 = " -agentlib:jdwp=transport=dt_android_adb,suspend=n,server=y";
            }
            sb.append(str5);
            str7 = sb.toString();
        }
        String format = String.format("NO_ADDR_COMPAT_LAYOUT_FIXUP=1 %sCLASSPATH=%s %s%s /system/bin%s %s", new Object[]{sb2.toString(), str, str3, str7, str8, str2});
        if (strArr == null) {
            return format;
        }
        StringBuilder sb6 = new StringBuilder(format);
        for (String append : strArr) {
            sb6.append(' ');
            sb6.append(append);
        }
        return sb6.toString();
    }

    /* JADX INFO: used method not loaded: eu.chainfire.librootjava.e.a(java.util.List):null, types can be incorrect */
    /* renamed from: a */
    public static List<String> m316a(Context context, Class<?> cls, String str, String str2, String[] strArr, String str3) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        String a = C0075a.m270a(context, str, arrayList, arrayList2, str2);
        StringBuilder sb = new StringBuilder();
        sb.append("#app_process=");
        sb.append(a);
        arrayList.add(0, sb.toString());
        C0079e.m294a((List<String>) arrayList);
        ArrayList arrayList3 = new ArrayList(arrayList);
        arrayList3.add(m313a(context, cls, a, strArr, str3));
        arrayList3.addAll(arrayList2);
        return arrayList3;
    }

    /* renamed from: a */
    public static void m317a() {
        C0077c.m282a();
    }

    /* renamed from: b */
    public static Context m318b() {
        return C0080f.m295a();
    }
}
