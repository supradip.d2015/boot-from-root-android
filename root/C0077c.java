package p002eu.chainfire.librootjava;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.system.Os;
import java.io.File;
import java.io.IOException;

/* renamed from: eu.chainfire.librootjava.c */
class C0077c {
    @TargetApi(21)
    /* renamed from: a */
    private static String m280a(String str) {
        return m284b() ? Os.getenv(str) : System.getenv(str);
    }

    /* renamed from: a */
    static String m281a(boolean z, String[] strArr) {
        String str;
        String str2;
        String str3;
        String str4;
        String a = m280a("LD_LIBRARY_PATH");
        if (m284b()) {
            StringBuilder sb = new StringBuilder();
            if (z) {
                str = "/system/lib64";
                str2 = "/data/lib64";
                str3 = "/vendor/lib64";
                str4 = "/data/vendor/lib64";
            } else {
                str = "/system/lib";
                str2 = "/data/lib";
                str3 = "/vendor/lib";
                str4 = "/data/vendor/lib";
            }
            for (String file : new String[]{str, str2, str3, str4}) {
                File file2 = new File(file);
                if (file2.exists()) {
                    try {
                        sb.append(file2.getCanonicalPath());
                        sb.append(':');
                        File[] listFiles = file2.listFiles();
                        if (listFiles != null) {
                            for (File file3 : listFiles) {
                                if (file3.isDirectory()) {
                                    sb.append(file3.getCanonicalPath());
                                    sb.append(':');
                                }
                            }
                        }
                    } catch (IOException unused) {
                    }
                }
            }
            if (strArr != null) {
                for (String append : strArr) {
                    sb.append(append);
                    sb.append(':');
                }
            }
            sb.append("/librootjava");
            if (a != null) {
                sb.append(':');
                sb.append(a);
            }
            return sb.toString();
        } else if (a != null) {
            return a;
        } else {
            return null;
        }
    }

    /* renamed from: a */
    static void m282a() {
        m283a((String) "LD_LIBRARY_PATH", m285c());
    }

    @TargetApi(21)
    /* renamed from: a */
    private static void m283a(String str, String str2) {
        if (!m284b()) {
            return;
        }
        if (str2 == null) {
            try {
                Os.unsetenv(str);
            } catch (Exception e) {
                C0078d.m287a(e);
            }
        } else {
            Os.setenv(str, str2, true);
        }
    }

    @TargetApi(23)
    /* renamed from: b */
    private static boolean m284b() {
        return VERSION.SDK_INT >= 24 || (VERSION.SDK_INT == 23 && VERSION.PREVIEW_SDK_INT != 0);
    }

    /* renamed from: c */
    private static String m285c() {
        String str = System.getenv("LD_LIBRARY_PATH");
        if (str == null || str.endsWith(":/librootjava")) {
            return null;
        }
        if (str.contains(":/librootjava:")) {
            str = str.substring(str.indexOf(":/librootjava:") + ":/librootjava:".length());
        }
        return str;
    }
}
