package p002eu.chainfire.librootjavadaemon;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: eu.chainfire.librootjavadaemon.IRootDaemonIPC */
public interface IRootDaemonIPC extends IInterface {

    /* renamed from: eu.chainfire.librootjavadaemon.IRootDaemonIPC$Stub */
    public static abstract class Stub extends Binder implements IRootDaemonIPC {
        private static final String DESCRIPTOR = "eu.chainfire.librootjavadaemon.IRootDaemonIPC";
        static final int TRANSACTION_broadcast = 3;
        static final int TRANSACTION_getVersion = 1;
        static final int TRANSACTION_terminate = 2;

        /* renamed from: eu.chainfire.librootjavadaemon.IRootDaemonIPC$Stub$Proxy */
        private static class Proxy implements IRootDaemonIPC {
            private IBinder mRemote;

            Proxy(IBinder iBinder) {
                this.mRemote = iBinder;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public void broadcast() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_broadcast, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public String getVersion() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readString();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void terminate() {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    this.mRemote.transact(Stub.TRANSACTION_terminate, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IRootDaemonIPC asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            return (queryLocalInterface == null || !(queryLocalInterface instanceof IRootDaemonIPC)) ? new Proxy(iBinder) : (IRootDaemonIPC) queryLocalInterface;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            if (i != 1598968902) {
                switch (i) {
                    case 1:
                        parcel.enforceInterface(DESCRIPTOR);
                        String version = getVersion();
                        parcel2.writeNoException();
                        parcel2.writeString(version);
                        return true;
                    case TRANSACTION_terminate /*2*/:
                        parcel.enforceInterface(DESCRIPTOR);
                        terminate();
                        parcel2.writeNoException();
                        return true;
                    case TRANSACTION_broadcast /*3*/:
                        parcel.enforceInterface(DESCRIPTOR);
                        broadcast();
                        parcel2.writeNoException();
                        return true;
                    default:
                        return super.onTransact(i, parcel, parcel2, i2);
                }
            } else {
                parcel2.writeString(DESCRIPTOR);
                return true;
            }
        }
    }

    void broadcast();

    String getVersion();

    void terminate();
}
