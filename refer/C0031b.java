package p002eu.chainfire.p005b;

import java.io.IOException;
import java.io.InputStream;

/* renamed from: eu.chainfire.b.b */
public class C0031b extends InputStream {

    /* renamed from: a */
    private final C0070d f115a;

    /* renamed from: b */
    private final InputStream f116b;

    /* renamed from: c */
    private final byte[] f117c;

    /* renamed from: d */
    private final int f118d;

    /* renamed from: e */
    private final int f119e;

    /* renamed from: f */
    private final byte[] f120f = new byte[1];

    /* renamed from: g */
    private final byte[] f121g = new byte[65536];

    /* renamed from: h */
    private int f122h = 0;

    /* renamed from: i */
    private volatile boolean f123i = false;

    /* renamed from: j */
    private volatile boolean f124j = false;

    public C0031b(C0070d dVar, String str) {
        this.f115a = dVar;
        this.f115a.mo159b();
        this.f116b = dVar.mo162e();
        this.f117c = str.getBytes("UTF-8");
        this.f118d = str.length();
        this.f119e = str.length() + 5;
    }

    /* renamed from: a */
    private void m113a(int i) {
        if (!mo100a()) {
            while (true) {
                try {
                    int available = this.f116b.available();
                    if (available <= 0 && i <= 0) {
                        break;
                    }
                    int length = this.f121g.length - this.f122h;
                    if (length != 0) {
                        int read = this.f116b.read(this.f121g, this.f122h, Math.max(i, Math.min(available, length)));
                        if (read < 0) {
                            mo101b();
                            break;
                        } else {
                            this.f122h += read;
                            i -= read;
                        }
                    } else {
                        return;
                    }
                } catch (IOException unused) {
                    mo101b();
                }
            }
        }
    }

    /* renamed from: a */
    public synchronized boolean mo100a() {
        return this.f123i;
    }

    /* renamed from: b */
    public synchronized void mo101b() {
        this.f123i = true;
    }

    public synchronized void close() {
        if (!mo100a() && !this.f124j) {
            do {
            } while (read(new byte[1024]) >= 0);
        }
    }

    public int read() {
        while (true) {
            int read = read(this.f120f, 0, 1);
            if (read < 0) {
                return -1;
            }
            if (read != 0) {
                return this.f120f[0] & 255;
            }
            try {
                Thread.sleep(16);
            } catch (InterruptedException unused) {
            }
        }
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public synchronized int read(byte[] bArr, int i, int i2) {
        int i3;
        boolean z;
        if (this.f124j) {
            return -1;
        }
        m113a(this.f118d - this.f122h);
        if (this.f122h < this.f118d) {
            return 0;
        }
        int max = Math.max(0, this.f122h - this.f119e);
        while (true) {
            if (max >= this.f122h - this.f118d) {
                max = -1;
                break;
            }
            int i4 = 0;
            while (true) {
                if (i4 >= this.f118d) {
                    z = true;
                    break;
                } else if (this.f121g[max + i4] != this.f117c[i4]) {
                    z = false;
                    break;
                } else {
                    i4++;
                }
            }
            if (z) {
                break;
            }
            max++;
        }
        if (max == 0) {
            while (this.f121g[this.f122h - 1] != 10) {
                if (!mo100a()) {
                    m113a(1);
                } else {
                    throw new IOException("EOF encountered, shell probably died");
                }
            }
            if (this.f115a.mo163f() != null) {
                this.f115a.mo163f().mo143a(new String(this.f121g, 0, this.f122h - 1, "UTF-8"));
            }
            this.f124j = true;
            return -1;
        }
        if (max != -1) {
            i3 = Math.min(i2, max);
        } else if (!mo100a()) {
            i3 = Math.min(i2, this.f122h - this.f119e);
        } else {
            throw new IOException("EOF encountered, shell probably died");
        }
        if (i3 > 0) {
            System.arraycopy(this.f121g, 0, bArr, i, i3);
            this.f122h -= i3;
            System.arraycopy(this.f121g, i3, this.f121g, 0, this.f122h);
        } else {
            try {
                Thread.sleep(4);
            } catch (Exception unused) {
            }
        }
        return i3;
    }
}
