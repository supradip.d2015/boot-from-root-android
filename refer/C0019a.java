package p002eu.chainfire.p003a.p004a;

import android.opengl.GLES20;
import android.opengl.Matrix;
import p002eu.chainfire.p003a.p004a.C0020b.C0021a;

/* renamed from: eu.chainfire.a.a.a */
public class C0019a {

    /* renamed from: a */
    private volatile int f54a;

    /* renamed from: b */
    private volatile int f55b;

    /* renamed from: c */
    private final float[] f56c = new float[16];

    public C0019a(int i, int i2, float[] fArr) {
        this.f54a = i;
        this.f55b = i2;
        mo79a(fArr);
    }

    /* renamed from: a */
    public static float[] m64a() {
        float[] fArr = new float[16];
        Matrix.setLookAtM(fArr, 0, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f);
        return fArr;
    }

    /* renamed from: a */
    public void mo76a(int i, int i2) {
        if (i >= 0) {
            this.f54a = i;
        }
        if (i2 >= 0) {
            this.f55b = i2;
        }
    }

    /* renamed from: a */
    public void mo77a(C0020b bVar, int i, int i2, int i3, int i4, C0021a aVar, float f) {
        float[] fArr = new float[16];
        float[] fArr2 = new float[16];
        float[] fArr3 = new float[16];
        Matrix.setIdentityM(fArr, 0);
        float f2 = ((float) i3) / ((float) this.f54a);
        float f3 = ((float) i4) / ((float) this.f55b);
        Matrix.translateM(fArr, 0, (((((float) i) / ((float) this.f54a)) * 2.0f) + f2) - 4.0f, 1.0f - (((((float) i2) / ((float) this.f55b)) * 2.0f) + f3), 0.0f);
        Matrix.scaleM(fArr, 0, f2, f3, 1.0f);
        Matrix.orthoM(fArr2, 0, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 10.0f);
        Matrix.multiplyMM(fArr3, 0, this.f56c, 0, fArr, 0);
        Matrix.multiplyMM(fArr3, 0, fArr2, 0, fArr3, 0);
        bVar.mo81a(fArr3, aVar, f);
    }

    /* renamed from: a */
    public void mo78a(boolean z) {
        if (!z) {
            GLES20.glDisable(3089);
        }
    }

    /* renamed from: a */
    public void mo79a(float[] fArr) {
        if (fArr != null) {
            System.arraycopy(fArr, 0, this.f56c, 0, Math.min(fArr.length, this.f56c.length));
        }
    }

    /* renamed from: a */
    public boolean mo80a(int i, int i2, int i3, int i4, int i5) {
        if (i5 == 0) {
            return true;
        }
        boolean glIsEnabled = GLES20.glIsEnabled(3089);
        if (!glIsEnabled) {
            GLES20.glEnable(3089);
        }
        GLES20.glScissor(i, (i5 - i2) - i4, i3, i4);
        return glIsEnabled;
    }
}
