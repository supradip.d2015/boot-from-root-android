package p002eu.chainfire.p003a.p004a;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Rect;
import android.opengl.GLES20;
import java.nio.FloatBuffer;

/* renamed from: eu.chainfire.a.a.b */
public class C0020b {

    /* renamed from: a */
    private static final float[] f57a = {0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f};

    /* renamed from: f */
    private static int f58f;

    /* renamed from: g */
    private static int[] f59g = {0, 0};

    /* renamed from: h */
    private static int[] f60h = {0, 0};

    /* renamed from: i */
    private static int[] f61i = {0, 0};

    /* renamed from: j */
    private static int[] f62j = {0, 0};

    /* renamed from: k */
    private static int[] f63k = {0, 0};

    /* renamed from: l */
    private static int[] f64l = {0, 0};

    /* renamed from: b */
    private boolean f65b = false;

    /* renamed from: c */
    private float[] f66c = new float[18];

    /* renamed from: d */
    private FloatBuffer f67d;

    /* renamed from: e */
    private FloatBuffer f68e;

    /* renamed from: m */
    private int f69m = 1;

    /* renamed from: n */
    private int f70n = 1;

    /* renamed from: o */
    private int f71o = 0;

    /* renamed from: p */
    private int f72p = 0;

    /* renamed from: q */
    private int f73q = f58f;

    /* renamed from: r */
    private Object f74r = null;

    /* renamed from: s */
    private int[] f75s;

    /* renamed from: t */
    private C0027e f76t = null;

    /* renamed from: u */
    private Bitmap f77u = null;

    /* renamed from: eu.chainfire.a.a.b$a */
    public enum C0021a {
        IMAGE,
        GLOBAL
    }

    public C0020b(C0027e eVar, Bitmap bitmap) {
        if (bitmap != null) {
            this.f76t = eVar;
            m71a(bitmap);
            this.f77u = bitmap.copy(Config.ARGB_8888, false);
        }
    }

    /* renamed from: a */
    public static void m70a() {
        int a = C0028f.m96a(35633, "uniform mat4 uMVPMatrix;attribute vec4 aPosition;attribute vec2 aTexCoords;varying vec2 vTexCoords;void main() {  vTexCoords = aTexCoords;  gl_Position = uMVPMatrix * aPosition;}");
        int a2 = C0028f.m96a(35632, "precision mediump float;uniform sampler2D uTexture;uniform float uAlpha;varying vec2 vTexCoords;void main() {  gl_FragColor = texture2D(uTexture, vTexCoords);  gl_FragColor.a = uAlpha;}");
        int a3 = C0028f.m96a(35632, "precision mediump float;uniform sampler2D uTexture;uniform float uAlpha;varying vec2 vTexCoords;void main() {  gl_FragColor = texture2D(uTexture, vTexCoords) * uAlpha;}");
        f59g[0] = C0028f.m95a(a, a2, null);
        f59g[1] = C0028f.m95a(a, a3, null);
        for (int i = 0; i < 2; i++) {
            f60h[i] = GLES20.glGetAttribLocation(f59g[i], "aPosition");
            f61i[i] = GLES20.glGetAttribLocation(f59g[i], "aTexCoords");
            f64l[i] = GLES20.glGetUniformLocation(f59g[i], "uMVPMatrix");
            f63k[i] = GLES20.glGetUniformLocation(f59g[i], "uTexture");
            f62j[i] = GLES20.glGetUniformLocation(f59g[i], "uAlpha");
        }
        int[] iArr = new int[1];
        GLES20.glGetIntegerv(3379, iArr, 0);
        f58f = iArr[0];
    }

    /* renamed from: a */
    private void m71a(Bitmap bitmap) {
        if (!this.f65b) {
            this.f73q = f58f;
            this.f65b = true;
            this.f67d = C0028f.m97a(this.f66c.length);
            this.f68e = C0028f.m98a(f57a);
            this.f71o = bitmap.getWidth();
            this.f72p = bitmap.getHeight();
            int i = this.f72p % this.f73q;
            this.f69m = (this.f71o / (this.f73q + 1)) + 1;
            this.f70n = (this.f72p / (this.f73q + 1)) + 1;
            this.f75s = new int[(this.f69m * this.f70n)];
            if (this.f69m == 1 && this.f70n == 1) {
                this.f75s[0] = this.f76t.mo96a(bitmap);
            } else {
                Rect rect = new Rect();
                for (int i2 = 0; i2 < this.f70n; i2++) {
                    int i3 = 0;
                    while (i3 < this.f69m) {
                        int i4 = i3 + 1;
                        rect.set(this.f73q * i3, ((this.f70n - i2) - 1) * this.f73q, this.f73q * i4, (this.f70n - i2) * this.f73q);
                        if (i > 0) {
                            rect.offset(0, (-this.f73q) + i);
                        }
                        rect.intersect(0, 0, this.f71o, this.f72p);
                        Bitmap createBitmap = Bitmap.createBitmap(bitmap, rect.left, rect.top, rect.width(), rect.height());
                        this.f75s[(this.f69m * i2) + i3] = this.f76t.mo96a(createBitmap);
                        createBitmap.recycle();
                        i3 = i4;
                    }
                }
            }
        }
    }

    /* JADX INFO: used method not loaded: eu.chainfire.a.a.e.a(int):null, types can be incorrect */
    /* renamed from: d */
    private void m72d() {
        if (this.f65b && this.f75s != null) {
            for (int a : this.f75s) {
                this.f76t.mo97a(a);
            }
            this.f75s = null;
            this.f65b = false;
        }
    }

    /* JADX INFO: used method not loaded: eu.chainfire.a.a.f.a(java.lang.String):null, types can be incorrect */
    /* renamed from: a */
    public void mo81a(float[] fArr, C0021a aVar, float f) {
        float f2;
        char c;
        mo82b();
        if (this.f65b) {
            GLES20.glEnable(3042);
            char c2 = 0;
            char c3 = 1;
            if ((f < 0.0f ? C0021a.IMAGE : aVar) == C0021a.IMAGE) {
                GLES20.glBlendFunc(1, 771);
                f2 = Math.abs(f);
                c = 1;
            } else {
                GLES20.glBlendFuncSeparate(770, 771, 1, 1);
                f2 = f;
                c = 0;
            }
            GLES20.glUseProgram(f59g[c]);
            GLES20.glUniformMatrix4fv(f64l[c], 1, false, fArr, 0);
            C0028f.m99a((String) "glUniformMatrix4fv");
            GLES20.glEnableVertexAttribArray(f60h[c]);
            GLES20.glVertexAttribPointer(f60h[c], 3, 5126, false, 12, this.f67d);
            GLES20.glActiveTexture(33984);
            GLES20.glUniform1i(f63k[c], 0);
            GLES20.glVertexAttribPointer(f61i[c], 2, 5126, false, 8, this.f68e);
            GLES20.glEnableVertexAttribArray(f61i[c]);
            GLES20.glUniform1f(f62j[c], f2);
            int i = 0;
            while (i < this.f70n) {
                int i2 = 0;
                while (i2 < this.f69m) {
                    float[] fArr2 = this.f66c;
                    float[] fArr3 = this.f66c;
                    float[] fArr4 = this.f66c;
                    float min = Math.min((((((float) i2) * 2.0f) * ((float) this.f73q)) / ((float) this.f71o)) - 4.0f, 1.0f);
                    fArr4[9] = min;
                    fArr3[3] = min;
                    fArr2[c2] = min;
                    float[] fArr5 = this.f66c;
                    float[] fArr6 = this.f66c;
                    float[] fArr7 = this.f66c;
                    float min2 = Math.min((((((float) (i + 1)) * 2.0f) * ((float) this.f73q)) / ((float) this.f72p)) - 4.0f, 1.0f);
                    fArr7[16] = min2;
                    fArr6[10] = min2;
                    fArr5[c3] = min2;
                    float[] fArr8 = this.f66c;
                    float[] fArr9 = this.f66c;
                    float[] fArr10 = this.f66c;
                    int i3 = i2 + 1;
                    float min3 = Math.min((((((float) i3) * 2.0f) * ((float) this.f73q)) / ((float) this.f71o)) - 4.0f, 1.0f);
                    fArr10[15] = min3;
                    fArr9[12] = min3;
                    fArr8[6] = min3;
                    float[] fArr11 = this.f66c;
                    float[] fArr12 = this.f66c;
                    float[] fArr13 = this.f66c;
                    float min4 = Math.min((((((float) i) * 2.0f) * ((float) this.f73q)) / ((float) this.f72p)) - 4.0f, 1.0f);
                    fArr13[13] = min4;
                    fArr12[7] = min4;
                    fArr11[4] = min4;
                    this.f67d.put(this.f66c);
                    this.f67d.position(0);
                    GLES20.glBindTexture(3553, this.f75s[(this.f69m * i) + i2]);
                    C0028f.m99a((String) "glBindTexture");
                    GLES20.glDrawArrays(4, 0, this.f66c.length / 3);
                    i2 = i3;
                    c2 = 0;
                    c3 = 1;
                }
                i++;
                c2 = 0;
                c3 = 1;
            }
            GLES20.glDisableVertexAttribArray(f60h[c]);
            GLES20.glDisableVertexAttribArray(f61i[c]);
        }
    }

    /* renamed from: b */
    public void mo82b() {
        m71a(this.f77u);
    }

    /* renamed from: c */
    public void mo83c() {
        if (this.f77u != null) {
            this.f77u.recycle();
            this.f77u = null;
        }
        m72d();
    }
}
