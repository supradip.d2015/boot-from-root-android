package p002eu.chainfire.p003a.p004a;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;

/* renamed from: eu.chainfire.a.a.d */
public class C0024d {

    /* renamed from: k */
    protected final C0027e f96k;

    /* renamed from: l */
    protected final C0019a f97l;

    /* renamed from: m */
    protected final Paint f98m = new Paint();

    /* renamed from: n */
    protected final Typeface f99n;

    /* renamed from: o */
    protected volatile int f100o;

    /* renamed from: p */
    protected volatile int f101p;

    /* renamed from: q */
    protected volatile int f102q;

    /* renamed from: r */
    protected volatile int f103r;

    /* renamed from: eu.chainfire.a.a.d$1 */
    static /* synthetic */ class C00251 {

        /* renamed from: a */
        static final /* synthetic */ int[] f104a = new int[C0026a.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                eu.chainfire.a.a.d$a[] r0 = p002eu.chainfire.p003a.p004a.C0024d.C0026a.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f104a = r0
                int[] r0 = f104a     // Catch:{ NoSuchFieldError -> 0x0014 }
                eu.chainfire.a.a.d$a r1 = p002eu.chainfire.p003a.p004a.C0024d.C0026a.LEFT     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f104a     // Catch:{ NoSuchFieldError -> 0x001f }
                eu.chainfire.a.a.d$a r1 = p002eu.chainfire.p003a.p004a.C0024d.C0026a.CENTER     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f104a     // Catch:{ NoSuchFieldError -> 0x002a }
                eu.chainfire.a.a.d$a r1 = p002eu.chainfire.p003a.p004a.C0024d.C0026a.RIGHT     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.p003a.p004a.C0024d.C00251.<clinit>():void");
        }
    }

    /* renamed from: eu.chainfire.a.a.d$a */
    public enum C0026a {
        LEFT,
        CENTER,
        RIGHT
    }

    protected C0024d(C0027e eVar, C0019a aVar, Typeface typeface, int i) {
        this.f96k = eVar;
        this.f97l = aVar;
        this.f99n = typeface;
        mo94a(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Bitmap mo92a(String str, int i, int i2, C0026a aVar, Bitmap bitmap) {
        Rect rect = new Rect();
        int i3 = 0;
        this.f98m.getTextBounds(str, 0, str.length(), rect);
        this.f98m.setColor(i);
        int i4 = (rect.right - rect.left) + (this.f103r * 2);
        if (i2 == 0) {
            i2 = i4;
        }
        if (bitmap == null) {
            bitmap = Bitmap.createBitmap(i2, this.f101p, Config.ARGB_8888);
        } else {
            i2 = bitmap.getWidth();
            bitmap.eraseColor(0);
        }
        switch (C00251.f104a[aVar.ordinal()]) {
            case 2:
                i3 = (i2 - i4) / 2;
                break;
            case 3:
                i3 = i2 - i4;
                break;
        }
        new Canvas(bitmap).drawText(str, (float) ((-rect.left) + this.f103r + i3), (float) ((-rect.top) + this.f102q), this.f98m);
        return bitmap;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C0020b mo93a(Bitmap bitmap) {
        return new C0020b(this.f96k, bitmap);
    }

    /* renamed from: a */
    public void mo94a(int i) {
        if (i >= 0) {
            this.f101p = i;
        }
        this.f100o = (int) Math.round(Math.ceil((double) (((float) this.f101p) / 1.2f)));
        this.f102q = (this.f101p - this.f100o) / 2;
        this.f103r = this.f101p / 4;
        this.f98m.setAntiAlias(true);
        this.f98m.setTypeface(this.f99n);
        this.f98m.setTextSize((float) this.f100o);
        this.f98m.setColor(-1);
        this.f98m.setShadowLayer(1.0f, 1.0f, 1.0f, -16777216);
    }

    /* renamed from: b */
    public void mo88b() {
    }
}
