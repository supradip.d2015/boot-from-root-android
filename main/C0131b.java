package p002eu.chainfire.liveboot.p006a;

import android.graphics.Color;
import android.os.Handler;
import android.os.Process;
import android.os.SystemClock;
import eu.chainfire.liveboot.R;
import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.concurrent.locks.ReentrantLock;
import p002eu.chainfire.librootjava.C0078d;
import p002eu.chainfire.p005b.C0032c.C0033a;
import p002eu.chainfire.p005b.C0032c.C0035c;
import p002eu.chainfire.p005b.C0070d.C0071a;

/* renamed from: eu.chainfire.liveboot.a.b */
public class C0131b {

    /* renamed from: a */
    public static final char[] f383a = {'V', 'D', 'I', 'W', 'E', 'F', 'S'};

    /* renamed from: b */
    public static final int[] f384b = {-1, Color.rgb(64, 128, 255), -16711936, -256, -65536, -65536, -16777216};

    /* renamed from: c */
    public static final int[] f385c = {R.string.logcat_level_verbose, R.string.logcat_level_debug, R.string.logcat_level_info, R.string.logcat_level_warning, R.string.logcat_level_error, R.string.logcat_level_fatal, R.string.logcat_level_silent};

    /* renamed from: d */
    public static final char[] f386d = {'M', 'S', 'R', 'E', 'C'};

    /* renamed from: e */
    public static final int[] f387e = {R.string.logcat_buffer_main, R.string.logcat_buffer_system, R.string.logcat_buffer_radio, R.string.logcat_buffer_events, R.string.logcat_buffer_crash};

    /* renamed from: f */
    public static final String[] f388f = {"main", "system", "radio", "events", "crash"};

    /* renamed from: g */
    public static final int[] f389g = {R.string.logcat_format_brief, R.string.logcat_format_process, R.string.logcat_format_tag, R.string.logcat_format_thread, R.string.logcat_format_time, R.string.logcat_format_threadtime};

    /* renamed from: h */
    public static final String[] f390h = {"brief", "process", "tag", "thread", "time", "threadtime"};

    /* renamed from: i */
    private static boolean[] f391i = {true, true, true, true, true, true, true};

    /* renamed from: j */
    private static boolean[] f392j = {true, true, true, true, true};

    /* renamed from: k */
    private static String f393k = "brief";

    /* renamed from: l */
    private final C0035c f394l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public final C0135c f395m;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public volatile long f396n = 0;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public volatile boolean f397o = false;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public final LinkedList<String> f398p = new LinkedList<>();
    /* access modifiers changed from: private */

    /* renamed from: q */
    public final int f399q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public volatile boolean f400r = false;

    /* renamed from: s */
    private final String[] f401s;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public final ReentrantLock f402t;

    public C0131b(C0135c cVar, int i, String str, String str2, String str3, Handler handler) {
        boolean z;
        boolean z2;
        boolean z3;
        StringBuilder sb = new StringBuilder();
        sb.append(" ");
        sb.append(String.valueOf(Process.myPid()));
        sb.append(")");
        StringBuilder sb2 = new StringBuilder();
        sb2.append("(");
        sb2.append(String.valueOf(Process.myPid()));
        sb2.append(")");
        this.f401s = new String[]{sb.toString(), sb2.toString(), "LiveBoot"};
        this.f402t = new ReentrantLock(true);
        if (str != null) {
            z = false;
            for (int i2 = 0; i2 <= 6; i2++) {
                boolean[] zArr = f391i;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("");
                sb3.append(f383a[i2]);
                zArr[i2] = str.contains(sb3.toString());
                z |= f391i[i2];
            }
        } else {
            z = true;
        }
        if (str2 != null) {
            z2 = false;
            for (int i3 = 0; i3 <= 4; i3++) {
                boolean[] zArr2 = f392j;
                StringBuilder sb4 = new StringBuilder();
                sb4.append("");
                sb4.append(f386d[i3]);
                zArr2[i3] = str2.contains(sb4.toString());
                z2 |= f392j[i3];
            }
        } else {
            z2 = true;
        }
        if (str3 != null) {
            z3 = false;
            for (String equals : f390h) {
                if (equals.equals(str3)) {
                    z3 = true;
                }
            }
        } else {
            z3 = false;
        }
        str3 = !z3 ? "brief" : str3;
        f393k = str3;
        String str4 = "";
        if (z && z2) {
            StringBuilder sb5 = new StringBuilder();
            sb5.append("logcat");
            sb5.append(" -v ");
            sb5.append(str3);
            for (int i4 = 0; i4 <= 4; i4++) {
                if (f392j[i4]) {
                    if (new File(String.format(Locale.ENGLISH, "/dev/log/%s", new Object[]{f388f[i4]})).exists()) {
                        sb5.append(" -b ");
                        sb5.append(f388f[i4]);
                    }
                }
            }
            str4 = sb5.toString();
        }
        this.f395m = cVar;
        this.f399q = i;
        this.f394l = new C0033a().mo113a(false).mo108a(handler).mo106a().mo109a((C0071a) new C0071a() {
            /* renamed from: a */
            public void mo143a(String str) {
                C0131b.this.f395m.onLog(this, str);
                if (!str.contains("libsuperuser") && !str.contains("SuperSU") && !str.contains("LiveBoot")) {
                    try {
                        C0131b.this.f402t.lock();
                        if (!C0131b.this.f397o) {
                            long uptimeMillis = SystemClock.uptimeMillis();
                            if (C0131b.this.f396n <= 0 || uptimeMillis - C0131b.this.f396n <= 16) {
                                C0131b.this.f396n = uptimeMillis;
                            } else {
                                if (C0131b.this.f400r) {
                                    C0131b.this.f397o = true;
                                    C0131b.this.m393c();
                                }
                                C0131b.this.f396n = 1;
                            }
                        }
                        if (C0131b.this.f397o) {
                            C0131b.this.m390a(str);
                        } else {
                            if (C0131b.this.f398p.size() >= C0131b.this.f399q) {
                                C0131b.this.f398p.pop();
                            }
                            C0131b.this.f398p.add(str);
                        }
                        C0131b.this.f402t.unlock();
                    } catch (Exception e) {
                        C0078d.m287a(e);
                    } catch (Throwable th) {
                        C0131b.this.f402t.unlock();
                        throw th;
                    }
                }
            }
        }).mo115b((C0071a) new C0071a() {
            /* renamed from: a */
            public void mo143a(String str) {
                C0131b.this.f395m.onLog(this, str);
                C0078d.m289a("logcat/stderr", "%s", str);
            }
        }).mo110a((Object) str4).mo118c();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m390a(java.lang.String r8) {
        /*
            r7 = this;
            int r0 = r8.length()
            if (r0 <= 0) goto L_0x00b7
            java.lang.String r0 = f393k
            java.lang.String r1 = "time"
            boolean r0 = r0.equals(r1)
            r1 = 6
            r2 = 0
            r3 = -1
            if (r0 == 0) goto L_0x0044
            r0 = 40
            int r0 = r8.indexOf(r0)
            if (r0 <= r3) goto L_0x0090
            r4 = 0
        L_0x001c:
            if (r4 > r1) goto L_0x0090
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = " "
            r5.append(r6)
            char[] r6 = f383a
            char r6 = r6[r4]
            r5.append(r6)
            java.lang.String r6 = "/"
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            int r5 = r8.indexOf(r5)
            if (r5 <= r3) goto L_0x0041
            if (r5 >= r0) goto L_0x0041
            goto L_0x0091
        L_0x0041:
            int r4 = r4 + 1
            goto L_0x001c
        L_0x0044:
            java.lang.String r0 = f393k
            java.lang.String r4 = "threadtime"
            boolean r0 = r0.equals(r4)
            if (r0 == 0) goto L_0x007f
            java.lang.String r0 = ": "
            int r0 = r8.indexOf(r0)
            if (r0 <= r3) goto L_0x0090
            r4 = 0
        L_0x0057:
            if (r4 > r1) goto L_0x0090
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = " "
            r5.append(r6)
            char[] r6 = f383a
            char r6 = r6[r4]
            r5.append(r6)
            java.lang.String r6 = " "
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            int r5 = r8.indexOf(r5)
            if (r5 <= r3) goto L_0x007c
            if (r5 >= r0) goto L_0x007c
            goto L_0x0091
        L_0x007c:
            int r4 = r4 + 1
            goto L_0x0057
        L_0x007f:
            char r0 = r8.charAt(r2)
            r4 = 0
        L_0x0084:
            if (r4 > r1) goto L_0x0090
            char[] r5 = f383a
            char r5 = r5[r4]
            if (r0 != r5) goto L_0x008d
            goto L_0x0091
        L_0x008d:
            int r4 = r4 + 1
            goto L_0x0084
        L_0x0090:
            r4 = -1
        L_0x0091:
            if (r4 <= r3) goto L_0x00b7
            boolean[] r0 = f391i
            boolean r0 = r0[r4]
            if (r0 == 0) goto L_0x00b7
            java.lang.String[] r0 = r7.f401s
            int r1 = r0.length
            r3 = 0
        L_0x009d:
            if (r3 >= r1) goto L_0x00ab
            r5 = r0[r3]
            boolean r5 = r8.contains(r5)
            if (r5 == 0) goto L_0x00a8
            goto L_0x00ac
        L_0x00a8:
            int r3 = r3 + 1
            goto L_0x009d
        L_0x00ab:
            r2 = 1
        L_0x00ac:
            if (r2 == 0) goto L_0x00b7
            eu.chainfire.liveboot.a.c r0 = r7.f395m
            int[] r1 = f384b
            r1 = r1[r4]
            r0.onLine(r7, r8, r1)
        L_0x00b7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.liveboot.p006a.C0131b.m390a(java.lang.String):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m393c() {
        Iterator it = this.f398p.iterator();
        while (it.hasNext()) {
            m390a((String) it.next());
        }
        this.f398p.clear();
    }

    /* renamed from: a */
    public void mo248a() {
        this.f402t.lock();
        try {
            this.f400r = true;
            m393c();
        } finally {
            this.f402t.unlock();
        }
    }

    /* renamed from: b */
    public void mo249b() {
        final C0035c cVar = this.f394l;
        new Thread(new Runnable() {
            public void run() {
                cVar.mo131f();
                cVar.close();
            }
        }).start();
    }
}
