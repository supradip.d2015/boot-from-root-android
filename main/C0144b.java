package p002eu.chainfire.liveboot;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.StatFs;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import p002eu.chainfire.librootjava.C0075a;
import p002eu.chainfire.librootjava.C0078d;
import p002eu.chainfire.p003a.C0012a;
import p002eu.chainfire.p005b.C0032c;
import p002eu.chainfire.p005b.C0032c.C0063q;
import p002eu.chainfire.p005b.C0073e;

/* renamed from: eu.chainfire.liveboot.b */
public class C0144b {

    /* renamed from: a */
    private static final String[] f445a = {"/system/su.d/0000liveboot"};

    /* renamed from: b */
    private static final String[] f446b = {"/system/etc/init.d/0000liveboot"};

    /* renamed from: c */
    private static final String[] f447c = {"/su/su.d/0000liveboot"};

    /* renamed from: d */
    private static final String[] f448d = {"/sbin/supersu/su.d/0000liveboot"};

    /* renamed from: e */
    private static final String[] f449e = {"/sbin/.core/img/.core/post-fs-data.d/0000liveboot", "/sbin/.core/img/.core/service.d/0000liveboot"};

    /* renamed from: f */
    private static final String[] f450f = {"/data/adb/post-fs-data.d/0000liveboot", "/data/adb/service.d/0000liveboot"};

    /* renamed from: eu.chainfire.liveboot.b$1 */
    static /* synthetic */ class C01451 {

        /* renamed from: a */
        static final /* synthetic */ int[] f451a = new int[C0147b.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                eu.chainfire.liveboot.b$b[] r0 = p002eu.chainfire.liveboot.C0144b.C0147b.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f451a = r0
                int[] r0 = f451a     // Catch:{ NoSuchFieldError -> 0x0014 }
                eu.chainfire.liveboot.b$b r1 = p002eu.chainfire.liveboot.C0144b.C0147b.SU_D     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f451a     // Catch:{ NoSuchFieldError -> 0x001f }
                eu.chainfire.liveboot.b$b r1 = p002eu.chainfire.liveboot.C0144b.C0147b.INIT_D     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f451a     // Catch:{ NoSuchFieldError -> 0x002a }
                eu.chainfire.liveboot.b$b r1 = p002eu.chainfire.liveboot.C0144b.C0147b.SU_SU_D     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f451a     // Catch:{ NoSuchFieldError -> 0x0035 }
                eu.chainfire.liveboot.b$b r1 = p002eu.chainfire.liveboot.C0144b.C0147b.SBIN_SU_D     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f451a     // Catch:{ NoSuchFieldError -> 0x0040 }
                eu.chainfire.liveboot.b$b r1 = p002eu.chainfire.liveboot.C0144b.C0147b.MAGISK_CORE     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = f451a     // Catch:{ NoSuchFieldError -> 0x004b }
                eu.chainfire.liveboot.b$b r1 = p002eu.chainfire.liveboot.C0144b.C0147b.MAGISK_ADB     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.liveboot.C0144b.C01451.<clinit>():void");
        }
    }

    /* renamed from: eu.chainfire.liveboot.b$a */
    private static class C0146a extends AsyncTask<Void, Integer, Void> {

        /* renamed from: a */
        private final Context f452a;

        /* renamed from: b */
        private final int f453b;

        /* renamed from: c */
        private final C0147b f454c;

        /* renamed from: d */
        private final Runnable f455d;

        /* renamed from: e */
        private ProgressDialog f456e;

        public C0146a(Context context, int i, C0147b bVar, Runnable runnable) {
            this.f452a = context;
            this.f453b = i;
            this.f454c = bVar;
            this.f455d = runnable;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            if (this.f453b == 1) {
                publishProgress(new Integer[]{Integer.valueOf(R.string.installing)});
                C0144b.m443c(this.f452a, this.f454c);
            } else if (this.f453b == 2) {
                publishProgress(new Integer[]{Integer.valueOf(R.string.uninstalling)});
                C0144b.m444d(this.f452a);
            }
            return null;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Void voidR) {
            try {
                this.f456e.dismiss();
                if (this.f455d != null) {
                    this.f455d.run();
                }
            } catch (Exception e) {
                C0078d.m287a(e);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onProgressUpdate(Integer... numArr) {
            this.f456e.setMessage(this.f452a.getString(numArr[0].intValue()));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.f456e = new ProgressDialog(this.f452a);
            this.f456e.setMessage(this.f452a.getString(R.string.loading));
            this.f456e.setIndeterminate(true);
            this.f456e.setProgressStyle(0);
            this.f456e.setCancelable(false);
            this.f456e.show();
        }
    }

    /* renamed from: eu.chainfire.liveboot.b$b */
    public enum C0147b {
        SU_D,
        INIT_D,
        SU_SU_D,
        SBIN_SU_D,
        MAGISK_CORE,
        MAGISK_ADB
    }

    /* renamed from: a */
    public static Context m430a(Context context) {
        return VERSION.SDK_INT >= 24 ? context.createDeviceProtectedStorageContext() : context;
    }

    /* renamed from: a */
    private static String m431a() {
        return m438a((String) "/su/bin/sush") ? "/su/bin/sush" : m438a((String) "/tmp-mksh/tmp-mksh") ? "/tmp-mksh/tmp-mksh" : "/system/bin/sh";
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0122  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0126  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x013f  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized java.util.List<java.lang.String> m432a(android.content.Context r8, boolean r9) {
        /*
            java.lang.Class<eu.chainfire.liveboot.b> r0 = p002eu.chainfire.liveboot.C0144b.class
            monitor-enter(r0)
            eu.chainfire.liveboot.d r1 = p002eu.chainfire.liveboot.C0149d.m455a(r8)     // Catch:{ all -> 0x0158 }
            android.content.Context r2 = m430a(r8)     // Catch:{ all -> 0x0158 }
            eu.chainfire.liveboot.d$d r8 = r1.f471g     // Catch:{ all -> 0x0158 }
            java.lang.String r8 = r8.mo271a()     // Catch:{ all -> 0x0158 }
            java.lang.String r3 = ""
            boolean r8 = r8.equals(r3)     // Catch:{ all -> 0x0158 }
            r3 = 1
            if (r8 != 0) goto L_0x002b
            eu.chainfire.liveboot.d$d r8 = r1.f472h     // Catch:{ all -> 0x0158 }
            java.lang.String r8 = r8.mo271a()     // Catch:{ all -> 0x0158 }
            java.lang.String r4 = ""
            boolean r8 = r8.equals(r4)     // Catch:{ all -> 0x0158 }
            if (r8 == 0) goto L_0x0029
            goto L_0x002b
        L_0x0029:
            r8 = 1
            goto L_0x002c
        L_0x002b:
            r8 = 0
        L_0x002c:
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ all -> 0x0158 }
            p002eu.chainfire.librootjava.C0079e.m293a(r3)     // Catch:{ all -> 0x0158 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ all -> 0x0158 }
            r3.<init>()     // Catch:{ all -> 0x0158 }
            java.lang.String r4 = r2.getPackageCodePath()     // Catch:{ all -> 0x0158 }
            r3.add(r4)     // Catch:{ all -> 0x0158 }
            if (r9 == 0) goto L_0x0044
            java.lang.String r4 = "boot"
            goto L_0x0046
        L_0x0044:
            java.lang.String r4 = "test"
        L_0x0046:
            r3.add(r4)     // Catch:{ all -> 0x0158 }
            eu.chainfire.liveboot.d$a r4 = r1.f469e     // Catch:{ all -> 0x0158 }
            boolean r4 = r4.mo266a()     // Catch:{ all -> 0x0158 }
            if (r4 == 0) goto L_0x0056
            java.lang.String r4 = "transparent"
            r3.add(r4)     // Catch:{ all -> 0x0158 }
        L_0x0056:
            eu.chainfire.liveboot.d$a r4 = r1.f470f     // Catch:{ all -> 0x0158 }
            boolean r4 = r4.mo266a()     // Catch:{ all -> 0x0158 }
            if (r4 == 0) goto L_0x0063
            java.lang.String r4 = "dark"
            r3.add(r4)     // Catch:{ all -> 0x0158 }
        L_0x0063:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0158 }
            r4.<init>()     // Catch:{ all -> 0x0158 }
            java.lang.String r5 = "logcatlevels="
            r4.append(r5)     // Catch:{ all -> 0x0158 }
            eu.chainfire.liveboot.d$d r5 = r1.f471g     // Catch:{ all -> 0x0158 }
            java.lang.String r5 = r5.mo271a()     // Catch:{ all -> 0x0158 }
            r4.append(r5)     // Catch:{ all -> 0x0158 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0158 }
            r3.add(r4)     // Catch:{ all -> 0x0158 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0158 }
            r4.<init>()     // Catch:{ all -> 0x0158 }
            java.lang.String r5 = "logcatbuffers="
            r4.append(r5)     // Catch:{ all -> 0x0158 }
            eu.chainfire.liveboot.d$d r5 = r1.f472h     // Catch:{ all -> 0x0158 }
            java.lang.String r5 = r5.mo271a()     // Catch:{ all -> 0x0158 }
            r4.append(r5)     // Catch:{ all -> 0x0158 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0158 }
            r3.add(r4)     // Catch:{ all -> 0x0158 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0158 }
            r4.<init>()     // Catch:{ all -> 0x0158 }
            java.lang.String r5 = "logcatformat="
            r4.append(r5)     // Catch:{ all -> 0x0158 }
            eu.chainfire.liveboot.d$d r5 = r1.f473i     // Catch:{ all -> 0x0158 }
            java.lang.String r5 = r5.mo271a()     // Catch:{ all -> 0x0158 }
            r4.append(r5)     // Catch:{ all -> 0x0158 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0158 }
            r3.add(r4)     // Catch:{ all -> 0x0158 }
            eu.chainfire.liveboot.d$a r4 = r1.f474j     // Catch:{ all -> 0x0158 }
            boolean r4 = r4.mo266a()     // Catch:{ all -> 0x0158 }
            if (r4 != 0) goto L_0x00be
            java.lang.String r4 = "logcatnocolors"
            r3.add(r4)     // Catch:{ all -> 0x0158 }
        L_0x00be:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0158 }
            r4.<init>()     // Catch:{ all -> 0x0158 }
            java.lang.String r5 = "dmesg="
            r4.append(r5)     // Catch:{ all -> 0x0158 }
            eu.chainfire.liveboot.d$a r5 = r1.f475k     // Catch:{ all -> 0x0158 }
            boolean r5 = r5.mo266a()     // Catch:{ all -> 0x0158 }
            if (r5 == 0) goto L_0x00d7
            if (r9 != 0) goto L_0x00d4
            if (r8 != 0) goto L_0x00d7
        L_0x00d4:
            java.lang.String r8 = "0-99"
            goto L_0x00d9
        L_0x00d7:
            java.lang.String r8 = "0--1"
        L_0x00d9:
            r4.append(r8)     // Catch:{ all -> 0x0158 }
            java.lang.String r8 = r4.toString()     // Catch:{ all -> 0x0158 }
            r3.add(r8)     // Catch:{ all -> 0x0158 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0158 }
            r8.<init>()     // Catch:{ all -> 0x0158 }
            java.lang.String r4 = "lines="
            r8.append(r4)     // Catch:{ all -> 0x0158 }
            eu.chainfire.liveboot.d$d r4 = r1.f476l     // Catch:{ all -> 0x0158 }
            java.lang.String r4 = r4.mo271a()     // Catch:{ all -> 0x0158 }
            r8.append(r4)     // Catch:{ all -> 0x0158 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0158 }
            r3.add(r8)     // Catch:{ all -> 0x0158 }
            eu.chainfire.liveboot.d$a r8 = r1.f477m     // Catch:{ all -> 0x0158 }
            boolean r8 = r8.mo266a()     // Catch:{ all -> 0x0158 }
            if (r8 == 0) goto L_0x010a
            java.lang.String r8 = "wordwrap"
            r3.add(r8)     // Catch:{ all -> 0x0158 }
        L_0x010a:
            eu.chainfire.liveboot.d$a r8 = r1.f478n     // Catch:{ all -> 0x0158 }
            boolean r8 = r8.mo266a()     // Catch:{ all -> 0x0158 }
            if (r8 == 0) goto L_0x0119
            if (r9 == 0) goto L_0x0119
            java.lang.String r8 = "save"
            r3.add(r8)     // Catch:{ all -> 0x0158 }
        L_0x0119:
            boolean r8 = p002eu.chainfire.librootjava.C0075a.m276d()     // Catch:{ all -> 0x0158 }
            if (r8 == 0) goto L_0x0122
            java.lang.String r8 = "/dev"
            goto L_0x0123
        L_0x0122:
            r8 = 0
        L_0x0123:
            r5 = r8
            if (r9 == 0) goto L_0x013f
            java.lang.Class<eu.chainfire.liveboot.a.d> r8 = p002eu.chainfire.liveboot.p006a.C0136d.class
            r4 = 0
            int r9 = r3.size()     // Catch:{ all -> 0x0158 }
            java.lang.String[] r9 = new java.lang.String[r9]     // Catch:{ all -> 0x0158 }
            java.lang.Object[] r9 = r3.toArray(r9)     // Catch:{ all -> 0x0158 }
            r6 = r9
            java.lang.String[] r6 = (java.lang.String[]) r6     // Catch:{ all -> 0x0158 }
            java.lang.String r7 = "eu.chainfire.liveboot:root"
            r3 = r8
            java.util.List r8 = p002eu.chainfire.librootjavadaemon.C0084a.m319a(r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0158 }
            monitor-exit(r0)
            return r8
        L_0x013f:
            java.lang.Class<eu.chainfire.liveboot.a.d> r8 = p002eu.chainfire.liveboot.p006a.C0136d.class
            r4 = 0
            int r9 = r3.size()     // Catch:{ all -> 0x0158 }
            java.lang.String[] r9 = new java.lang.String[r9]     // Catch:{ all -> 0x0158 }
            java.lang.Object[] r9 = r3.toArray(r9)     // Catch:{ all -> 0x0158 }
            r6 = r9
            java.lang.String[] r6 = (java.lang.String[]) r6     // Catch:{ all -> 0x0158 }
            java.lang.String r7 = "eu.chainfire.liveboot:root"
            r3 = r8
            java.util.List r8 = p002eu.chainfire.librootjava.C0083h.m316a(r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0158 }
            monitor-exit(r0)
            return r8
        L_0x0158:
            r8 = move-exception
            monitor-exit(r0)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: p002eu.chainfire.liveboot.C0144b.m432a(android.content.Context, boolean):java.util.List");
    }

    /* renamed from: a */
    public static void m433a(Activity activity, C0147b bVar, Runnable runnable) {
        new C0146a(activity, 1, bVar, runnable).execute(new Void[0]);
    }

    /* renamed from: a */
    public static void m434a(Activity activity, Runnable runnable) {
        new C0146a(activity, 2, null, runnable).execute(new Void[0]);
    }

    /* renamed from: a */
    public static boolean m435a(long j, int i) {
        try {
            StatFs statFs = new StatFs("/system");
            long blockSizeLong = (j / statFs.getBlockSizeLong()) + ((long) (i * 3));
            return statFs.getAvailableBlocksLong() >= blockSizeLong || statFs.getFreeBlocksLong() >= blockSizeLong;
        } catch (Exception unused) {
            return true;
        }
    }

    /* renamed from: a */
    public static boolean m436a(Context context, C0147b bVar) {
        boolean z;
        String absolutePath = m430a(context).getFilesDir().getAbsolutePath();
        boolean z2 = true;
        for (String str : m439a(bVar)) {
            List<String> a = C0063q.m241a(String.format(Locale.ENGLISH, "cat %s", new Object[]{str}));
            if (a != null) {
                z = false;
                for (String contains : a) {
                    if (contains.contains(String.format(Locale.ENGLISH, "%s/liveboot", new Object[]{absolutePath}))) {
                        z = true;
                    }
                }
            } else {
                z = false;
            }
            z2 = z2 && z;
        }
        return !z2;
    }

    /* renamed from: a */
    public static boolean m437a(C0149d dVar) {
        int a = dVar.f468d.mo267a();
        return a == 0 || a < 182;
    }

    /* renamed from: a */
    private static boolean m438a(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" -c \"echo OK\"");
        List<String> a = C0032c.m116a("su", new String[]{sb.toString()}, null, true);
        if (a != null) {
            for (String contains : a) {
                if (contains.contains("OK")) {
                    return true;
                }
            }
        }
        return false;
    }

    /* renamed from: a */
    public static String[] m439a(C0147b bVar) {
        switch (C01451.f451a[bVar.ordinal()]) {
            case C0012a.reservedArgs /*1*/:
                return f445a;
            case 2:
                return f446b;
            case 3:
                return f447c;
            case 4:
                return f448d;
            case 5:
                return f449e;
            case 6:
                return f450f;
            default:
                return null;
        }
    }

    /* renamed from: b */
    public static boolean m440b(Context context) {
        String absolutePath = m430a(context).getFilesDir().getAbsolutePath();
        return !new File(String.format(Locale.ENGLISH, "%s/liveboot", new Object[]{absolutePath})).exists();
    }

    @SuppressLint({"SdCardPath"})
    /* renamed from: b */
    public static boolean m441b(Context context, C0147b bVar) {
        return m437a(C0149d.m455a(context)) || m440b(context) || m436a(context, bVar);
    }

    /* renamed from: c */
    public static void m442c(Context context) {
        Context a = m430a(context);
        String absolutePath = a.getFilesDir().getAbsolutePath();
        String c = C0075a.m275c();
        ArrayList arrayList = new ArrayList();
        Locale locale = Locale.ENGLISH;
        StringBuilder sb = new StringBuilder();
        sb.append(C0073e.m267a("rm", new Object[0]));
        sb.append(" %s/app_process");
        arrayList.add(String.format(locale, sb.toString(), new Object[]{absolutePath}));
        Locale locale2 = Locale.ENGLISH;
        StringBuilder sb2 = new StringBuilder();
        sb2.append(C0073e.m267a("rm", new Object[0]));
        sb2.append(" %s/liveboot");
        arrayList.add(String.format(locale2, sb2.toString(), new Object[]{absolutePath}));
        Locale locale3 = Locale.ENGLISH;
        StringBuilder sb3 = new StringBuilder();
        sb3.append(C0073e.m267a("cp", new Object[0]));
        sb3.append(" %s %s/app_process");
        char c2 = 2;
        arrayList.add(String.format(locale3, sb3.toString(), new Object[]{c, absolutePath}));
        Locale locale4 = Locale.ENGLISH;
        StringBuilder sb4 = new StringBuilder();
        sb4.append(C0073e.m267a("chown", new Object[0]));
        sb4.append(" 0.0 %s/app_process");
        arrayList.add(String.format(locale4, sb4.toString(), new Object[]{absolutePath}));
        Locale locale5 = Locale.ENGLISH;
        StringBuilder sb5 = new StringBuilder();
        sb5.append(C0073e.m267a("chmod", new Object[0]));
        sb5.append(" 0700 %s/app_process");
        arrayList.add(String.format(locale5, sb5.toString(), new Object[]{absolutePath}));
        Locale locale6 = Locale.ENGLISH;
        StringBuilder sb6 = new StringBuilder();
        sb6.append(C0073e.m267a("chcon", new Object[0]));
        sb6.append(" u:object_r:app_data_file:s0 %s/app_process");
        arrayList.add(String.format(locale6, sb6.toString(), new Object[]{absolutePath}));
        String str = null;
        if (VERSION.SDK_INT == 19) {
            String a2 = C0073e.m267a("id", new Object[0]);
            StringBuilder sb7 = new StringBuilder();
            sb7.append("sh -c \"");
            sb7.append(a2);
            sb7.append("\"");
            List<String> a3 = C0032c.m116a("su --context u:r:recovery:s0", new String[]{a2, sb7.toString()}, null, false);
            if (a3 != null) {
                Iterator<String> it = a3.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    String next = it.next();
                    if (!next.contains("u:r:recovery:s0")) {
                        if (next.contains("u:r:init_shell:s0")) {
                            str = "u:r:init_shell:s0";
                            break;
                        }
                    } else {
                        str = "u:r:recovery:s0";
                        break;
                    }
                }
            }
        }
        String a4 = m431a();
        String[] strArr = {"liveboot", "test"};
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            String str2 = strArr[i];
            Locale locale7 = Locale.ENGLISH;
            int i2 = 3;
            Object[] objArr = new Object[3];
            objArr[0] = a4;
            objArr[1] = absolutePath;
            objArr[c2] = str2;
            arrayList.add(String.format(locale7, "echo '#!%s' > %s/%s", objArr));
            if (str != null) {
                Locale locale8 = Locale.ENGLISH;
                Object[] objArr2 = new Object[3];
                objArr2[0] = str;
                objArr2[1] = absolutePath;
                objArr2[c2] = str2;
                arrayList.add(String.format(locale8, "echo 'echo \"%s\" > /proc/self/attr/current' >> %s/%s", objArr2));
            }
            for (String str3 : m432a(a, str2.equals("liveboot"))) {
                Locale locale9 = Locale.ENGLISH;
                Context context2 = a;
                Object[] objArr3 = new Object[i2];
                objArr3[0] = str3;
                objArr3[1] = absolutePath;
                objArr3[2] = str2;
                arrayList.add(String.format(locale9, "echo '%s' >> %s/%s", objArr3));
                Locale locale10 = Locale.ENGLISH;
                Object[] objArr4 = new Object[i2];
                objArr4[0] = C0073e.m267a("chmod", new Object[0]);
                objArr4[1] = absolutePath;
                objArr4[2] = str2;
                arrayList.add(String.format(locale10, "%s 0700 %s/%s", objArr4));
                a = context2;
                i2 = 3;
            }
            Context context3 = a;
            i++;
            c2 = 2;
        }
        C0063q.m242a((List<String>) arrayList);
    }

    /* renamed from: c */
    public static void m443c(Context context, C0147b bVar) {
        String[] strArr;
        String[] strArr2;
        String[] strArr3;
        String[] strArr4;
        C0147b bVar2 = bVar;
        C0149d a = C0149d.m455a(context);
        Context a2 = m430a(context);
        String absolutePath = a2.getFilesDir().getAbsolutePath();
        String a3 = m431a();
        m442c(a2);
        ArrayList arrayList = new ArrayList();
        if (bVar2 == C0147b.SU_D || bVar2 == C0147b.INIT_D) {
            arrayList.add("mount -o rw,remount /system");
            arrayList.add("mount -o rw,remount /system /system");
            for (String str : f445a) {
                Locale locale = Locale.ENGLISH;
                StringBuilder sb = new StringBuilder();
                sb.append(C0073e.m267a("rm", new Object[0]));
                sb.append(" %s");
                arrayList.add(String.format(locale, sb.toString(), new Object[]{str}));
            }
            for (String str2 : f446b) {
                Locale locale2 = Locale.ENGLISH;
                StringBuilder sb2 = new StringBuilder();
                sb2.append(C0073e.m267a("rm", new Object[0]));
                sb2.append(" %s");
                arrayList.add(String.format(locale2, sb2.toString(), new Object[]{str2}));
            }
        }
        int i = 2;
        if (bVar2 == C0147b.SU_D) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(C0073e.m267a("mkdir", new Object[0]));
            sb3.append(" /system/su.d");
            arrayList.add(sb3.toString());
            StringBuilder sb4 = new StringBuilder();
            sb4.append(C0073e.m267a("chown", new Object[0]));
            sb4.append(" 0.0 /system/su.d");
            arrayList.add(sb4.toString());
            StringBuilder sb5 = new StringBuilder();
            sb5.append(C0073e.m267a("chmod", new Object[0]));
            sb5.append(" 0700 /system/su.d");
            arrayList.add(sb5.toString());
            String[] strArr5 = f445a;
            int length = strArr5.length;
            int i2 = 0;
            while (i2 < length) {
                String str3 = strArr5[i2];
                Locale locale3 = Locale.ENGLISH;
                Object[] objArr = new Object[i];
                objArr[0] = a3;
                objArr[1] = str3;
                arrayList.add(String.format(locale3, "echo '#!%s' > %s", objArr));
                arrayList.add(String.format(Locale.ENGLISH, "echo '%s %s/liveboot &' >> %s", new Object[]{a3, absolutePath, str3}));
                Locale locale4 = Locale.ENGLISH;
                StringBuilder sb6 = new StringBuilder();
                sb6.append(C0073e.m267a("chown", new Object[0]));
                sb6.append(" 0.0 %s");
                arrayList.add(String.format(locale4, sb6.toString(), new Object[]{str3}));
                Locale locale5 = Locale.ENGLISH;
                StringBuilder sb7 = new StringBuilder();
                sb7.append(C0073e.m267a("chmod", new Object[0]));
                sb7.append(" 0700 %s");
                arrayList.add(String.format(locale5, sb7.toString(), new Object[]{str3}));
                i2++;
                i = 2;
            }
        } else if (bVar2 == C0147b.INIT_D) {
            for (String str4 : f446b) {
                arrayList.add(String.format(Locale.ENGLISH, "echo '#!/system/bin/sh' > %s", new Object[]{str4}));
                arrayList.add(String.format(Locale.ENGLISH, "echo '/system/bin/sh %s/liveboot &' >> %s", new Object[]{absolutePath, str4}));
                Locale locale6 = Locale.ENGLISH;
                StringBuilder sb8 = new StringBuilder();
                sb8.append(C0073e.m267a("chown", new Object[0]));
                sb8.append(" 0.0 %s");
                arrayList.add(String.format(locale6, sb8.toString(), new Object[]{str4}));
                Locale locale7 = Locale.ENGLISH;
                StringBuilder sb9 = new StringBuilder();
                sb9.append(C0073e.m267a("chmod", new Object[0]));
                sb9.append(" 0700 %s");
                arrayList.add(String.format(locale7, sb9.toString(), new Object[]{str4}));
            }
        } else if (bVar2 == C0147b.SU_SU_D) {
            StringBuilder sb10 = new StringBuilder();
            sb10.append(C0073e.m267a("mkdir", new Object[0]));
            sb10.append(" /su/su.d");
            arrayList.add(sb10.toString());
            StringBuilder sb11 = new StringBuilder();
            sb11.append(C0073e.m267a("chown", new Object[0]));
            sb11.append(" 0.0 /su/su.d");
            arrayList.add(sb11.toString());
            StringBuilder sb12 = new StringBuilder();
            sb12.append(C0073e.m267a("chmod", new Object[0]));
            sb12.append(" 0700 /su/su.d");
            arrayList.add(sb12.toString());
            for (String str5 : f447c) {
                arrayList.add(String.format(Locale.ENGLISH, "echo '#!%s' > %s", new Object[]{a3, str5}));
                arrayList.add(String.format(Locale.ENGLISH, "echo '%s %s/liveboot &' >> %s", new Object[]{a3, absolutePath, str5}));
                Locale locale8 = Locale.ENGLISH;
                StringBuilder sb13 = new StringBuilder();
                sb13.append(C0073e.m267a("chown", new Object[0]));
                sb13.append(" 0.0 %s");
                arrayList.add(String.format(locale8, sb13.toString(), new Object[]{str5}));
                Locale locale9 = Locale.ENGLISH;
                StringBuilder sb14 = new StringBuilder();
                sb14.append(C0073e.m267a("chmod", new Object[0]));
                sb14.append(" 0700 %s");
                arrayList.add(String.format(locale9, sb14.toString(), new Object[]{str5}));
            }
        } else if (bVar2 == C0147b.SBIN_SU_D) {
            StringBuilder sb15 = new StringBuilder();
            sb15.append(C0073e.m267a("mkdir", new Object[0]));
            sb15.append(" /sbin/supersu/su.d");
            arrayList.add(sb15.toString());
            StringBuilder sb16 = new StringBuilder();
            sb16.append(C0073e.m267a("chown", new Object[0]));
            sb16.append(" 0.0 /sbin/supersu/su.d");
            arrayList.add(sb16.toString());
            StringBuilder sb17 = new StringBuilder();
            sb17.append(C0073e.m267a("chmod", new Object[0]));
            sb17.append(" 0700 /sbin/supersu/su.d");
            arrayList.add(sb17.toString());
            for (String str6 : f448d) {
                arrayList.add(String.format(Locale.ENGLISH, "echo '#!%s' > %s", new Object[]{a3, str6}));
                arrayList.add(String.format(Locale.ENGLISH, "echo '%s %s/liveboot &' >> %s", new Object[]{a3, absolutePath, str6}));
                Locale locale10 = Locale.ENGLISH;
                StringBuilder sb18 = new StringBuilder();
                sb18.append(C0073e.m267a("chown", new Object[0]));
                sb18.append(" 0.0 %s");
                arrayList.add(String.format(locale10, sb18.toString(), new Object[]{str6}));
                Locale locale11 = Locale.ENGLISH;
                StringBuilder sb19 = new StringBuilder();
                sb19.append(C0073e.m267a("chmod", new Object[0]));
                sb19.append(" 0700 %s");
                arrayList.add(String.format(locale11, sb19.toString(), new Object[]{str6}));
            }
        } else if (bVar2 == C0147b.MAGISK_CORE || bVar2 == C0147b.MAGISK_ADB) {
            for (String str7 : bVar2 == C0147b.MAGISK_CORE ? f449e : f450f) {
                arrayList.add(String.format(Locale.ENGLISH, "echo '#!%s' > %s", new Object[]{a3, str7}));
                arrayList.add(String.format(Locale.ENGLISH, "echo '{' >> %s", new Object[]{str7}));
                arrayList.add(String.format(Locale.ENGLISH, "echo '    while (true); do' >> %s", new Object[]{str7}));
                arrayList.add(String.format(Locale.ENGLISH, "echo '        if [ -d \"%s\" ]; then' >> %s", new Object[]{absolutePath, str7}));
                arrayList.add(String.format(Locale.ENGLISH, "echo '            break;' >> %s", new Object[]{str7}));
                arrayList.add(String.format(Locale.ENGLISH, "echo '        fi' >> %s", new Object[]{str7}));
                arrayList.add(String.format(Locale.ENGLISH, "echo '        sleep 0.1' >> %s", new Object[]{str7}));
                arrayList.add(String.format(Locale.ENGLISH, "echo '    done' >> %s", new Object[]{str7}));
                arrayList.add(String.format(Locale.ENGLISH, "echo '    %s %s/liveboot' >> %s", new Object[]{a3, absolutePath, str7}));
                arrayList.add(String.format(Locale.ENGLISH, "echo '} &' >> %s", new Object[]{str7}));
                Locale locale12 = Locale.ENGLISH;
                StringBuilder sb20 = new StringBuilder();
                sb20.append(C0073e.m267a("chown", new Object[0]));
                sb20.append(" 0.0 %s");
                arrayList.add(String.format(locale12, sb20.toString(), new Object[]{str7}));
                Locale locale13 = Locale.ENGLISH;
                StringBuilder sb21 = new StringBuilder();
                sb21.append(C0073e.m267a("chmod", new Object[0]));
                sb21.append(" 0700 %s");
                arrayList.add(String.format(locale13, sb21.toString(), new Object[]{str7}));
            }
        }
        if (bVar2 == C0147b.SU_D || bVar2 == C0147b.INIT_D) {
            arrayList.add("mount -o ro,remount /system /system");
            arrayList.add("mount -o ro,remount /system");
        }
        C0063q.m242a((List<String>) arrayList);
        if (!m436a(a2, bVar2)) {
            a.f468d.mo268a(m445e(a2));
        }
    }

    /* renamed from: d */
    public static void m444d(Context context) {
        boolean z;
        String[][] strArr;
        ArrayList arrayList = new ArrayList();
        for (String append : f445a) {
            StringBuilder sb = new StringBuilder();
            sb.append("ls -l ");
            sb.append(append);
            arrayList.add(sb.toString());
        }
        for (String append2 : f446b) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("ls -l ");
            sb2.append(append2);
            arrayList.add(sb2.toString());
        }
        List<String> a = C0032c.m116a("su", (String[]) arrayList.toArray(new String[arrayList.size()]), null, false);
        if (a != null) {
            Iterator<String> it = a.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().contains("liveboot")) {
                        z = true;
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        z = false;
        ArrayList arrayList2 = new ArrayList();
        if (z) {
            arrayList2.add("mount -o rw,remount /system");
            arrayList2.add("mount -o rw,remount /system /system");
        }
        for (String[] strArr2 : new String[][]{f445a, f446b, f447c, f448d, f449e, f450f}) {
            for (String str : strArr[r5]) {
                Locale locale = Locale.ENGLISH;
                StringBuilder sb3 = new StringBuilder();
                sb3.append(C0073e.m267a("rm", new Object[0]));
                sb3.append(" %s");
                arrayList2.add(String.format(locale, sb3.toString(), new Object[]{str}));
            }
        }
        if (z) {
            arrayList2.add("mount -o ro,remount /system /system");
            arrayList2.add("mount -o ro,remount /system");
        }
        C0063q.m242a((List<String>) arrayList2);
    }

    /* renamed from: e */
    private static int m445e(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (Exception e) {
            C0078d.m287a(e);
            return 0;
        }
    }
}
