package p002eu.chainfire.liveboot;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.android.vending.billing.IInAppBillingService;
import com.android.vending.billing.IInAppBillingService.Stub;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;
import p002eu.chainfire.librootjava.C0078d;

/* renamed from: eu.chainfire.liveboot.a */
public class C0121a implements Closeable {

    /* renamed from: a */
    private static final String[] f335a = {"purchase.2"};

    /* renamed from: b */
    private static final String[] f336b = new String[0];

    /* renamed from: c */
    private final Context f337c;

    /* renamed from: d */
    private final boolean f338d;

    /* renamed from: e */
    private final ArrayList<C0123a> f339e = new ArrayList<>();

    /* renamed from: f */
    private final ArrayList<C0125c> f340f = new ArrayList<>();
    /* access modifiers changed from: private */

    /* renamed from: g */
    public volatile IInAppBillingService f341g = null;

    /* renamed from: h */
    private ServiceConnection f342h = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            C0121a.this.f341g = Stub.asInterface(iBinder);
        }

        public void onServiceDisconnected(ComponentName componentName) {
            C0121a.this.f341g = null;
        }
    };

    /* renamed from: eu.chainfire.liveboot.a$a */
    public class C0123a {
        /* access modifiers changed from: private */

        /* renamed from: b */
        public String f345b;

        /* renamed from: c */
        private C0124b f346c;

        /* renamed from: d */
        private String f347d;

        /* renamed from: e */
        private long f348e;

        /* renamed from: f */
        private String f349f;

        /* renamed from: g */
        private String f350g;

        /* renamed from: h */
        private String f351h;

        public C0123a(String str) {
            JSONObject jSONObject = new JSONObject(str);
            this.f345b = jSONObject.getString("productId");
            this.f346c = jSONObject.getString("type").equals("subs") ? C0124b.SUBSCRIPTION : C0124b.ONCE;
            this.f347d = jSONObject.getString("price");
            this.f348e = jSONObject.getLong("price_amount_micros");
            this.f349f = jSONObject.getString("price_currency_code");
            this.f350g = jSONObject.getString("title");
            this.f351h = jSONObject.getString("description");
            C0078d.m289a("IAP", "InAppPurchase: productId[%s] type[%s] price[%s] priceMicros[%d] priceCurrency[%s] title[%s] description[%s]", this.f345b, this.f346c, this.f347d, Long.valueOf(this.f348e), this.f349f, this.f350g, this.f351h);
        }

        /* renamed from: a */
        public String mo241a() {
            return this.f345b;
        }

        /* renamed from: b */
        public C0124b mo242b() {
            return this.f346c;
        }
    }

    /* renamed from: eu.chainfire.liveboot.a$b */
    public enum C0124b {
        ONCE,
        SUBSCRIPTION
    }

    /* renamed from: eu.chainfire.liveboot.a$c */
    public class C0125c {

        /* renamed from: b */
        private String f356b;

        /* renamed from: c */
        private String f357c;

        /* renamed from: d */
        private String f358d;

        /* renamed from: e */
        private long f359e;

        /* renamed from: f */
        private C0126d f360f;

        /* renamed from: g */
        private String f361g;

        /* renamed from: h */
        private String f362h;

        public C0125c(String str) {
            C0126d dVar;
            JSONObject jSONObject = new JSONObject(str);
            this.f356b = jSONObject.getString("orderId");
            this.f357c = jSONObject.getString("packageName");
            this.f358d = jSONObject.getString("productId");
            this.f359e = jSONObject.getLong("purchaseTime");
            switch (jSONObject.getInt("purchaseState")) {
                case 0:
                    dVar = C0126d.PURCHASED;
                    break;
                case 2:
                    dVar = C0126d.REFUNDED;
                    break;
                default:
                    dVar = C0126d.CANCELED;
                    break;
            }
            this.f360f = dVar;
            this.f361g = jSONObject.getString("developerPayload");
            this.f362h = jSONObject.getString("purchaseToken");
            C0078d.m289a("IAP", "Order: orderId[%s] productId[%s] purchaseState[%s]", this.f356b, this.f358d, this.f360f);
        }

        /* renamed from: a */
        public String mo243a() {
            return this.f358d;
        }

        /* renamed from: b */
        public C0126d mo244b() {
            return this.f360f;
        }
    }

    /* renamed from: eu.chainfire.liveboot.a$d */
    public enum C0126d {
        PURCHASED,
        CANCELED,
        REFUNDED
    }

    public C0121a(Context context) {
        boolean z;
        this.f337c = context;
        try {
            Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
            intent.setPackage("com.android.vending");
            z = context.bindService(intent, this.f342h, 1);
        } catch (Exception unused) {
            z = false;
        }
        this.f338d = z;
    }

    /* renamed from: a */
    private Bundle m357a(String[] strArr) {
        ArrayList arrayList = new ArrayList();
        for (String add : strArr) {
            arrayList.add(add);
        }
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("ITEM_ID_LIST", arrayList);
        return bundle;
    }

    /* renamed from: a */
    public C0123a mo232a(String str) {
        Iterator<C0123a> it = this.f339e.iterator();
        while (it.hasNext()) {
            C0123a next = it.next();
            if (next.mo241a().equals(str)) {
                return next;
            }
        }
        return null;
    }

    /* renamed from: a */
    public boolean mo233a() {
        return this.f338d;
    }

    /* renamed from: a */
    public boolean mo234a(C0123a aVar, Activity activity, int i) {
        try {
            Bundle buyIntent = this.f341g.getBuyIntent(3, this.f337c.getPackageName(), aVar.f345b, aVar.mo242b() == C0124b.ONCE ? "inapp" : "subs", "nuclearPayloadsGoBoom");
            if (buyIntent != null && buyIntent.getInt("RESPONSE_CODE") == 0) {
                activity.startIntentSenderForResult(((PendingIntent) buyIntent.getParcelable("BUY_INTENT")).getIntentSender(), i, new Intent(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue());
                return true;
            }
        } catch (SendIntentException | RemoteException e) {
            C0078d.m287a((Exception) e);
        }
        return false;
    }

    /* renamed from: a */
    public C0125c[] mo235a(String str, boolean z) {
        ArrayList arrayList = new ArrayList();
        Iterator<C0125c> it = this.f340f.iterator();
        while (it.hasNext()) {
            C0125c next = it.next();
            if ((str == null || next.mo243a().equals(str)) && (z || next.mo244b().equals(C0126d.PURCHASED))) {
                arrayList.add(next);
            }
        }
        return (C0125c[]) arrayList.toArray(new C0125c[arrayList.size()]);
    }

    /* renamed from: b */
    public boolean mo236b() {
        return this.f341g != null;
    }

    /* renamed from: c */
    public boolean mo237c() {
        Bundle[] bundleArr;
        String[] strArr;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        try {
            for (Bundle bundle : new Bundle[]{this.f341g.getSkuDetails(3, this.f337c.getPackageName(), "inapp", m357a(f335a)), this.f341g.getSkuDetails(3, this.f337c.getPackageName(), "subs", m357a(f336b))}) {
                if (bundle != null) {
                    ArrayList<String> stringArrayList = bundle.getStringArrayList("DETAILS_LIST");
                    if (stringArrayList != null) {
                        Iterator<String> it = stringArrayList.iterator();
                        while (it.hasNext()) {
                            arrayList.add(new C0123a(it.next()));
                        }
                    }
                }
            }
            for (String str : new String[]{"inapp", "subs"}) {
                String str2 = null;
                while (true) {
                    Bundle purchases = this.f341g.getPurchases(3, this.f337c.getPackageName(), str, str2);
                    if (purchases != null) {
                        if (purchases.getInt("RESPONSE_CODE", -1) == 0) {
                            ArrayList<String> stringArrayList2 = purchases.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                            if (stringArrayList2 != null) {
                                Iterator<String> it2 = stringArrayList2.iterator();
                                while (it2.hasNext()) {
                                    arrayList2.add(new C0125c(it2.next()));
                                }
                                str2 = purchases.getString("INAPP_CONTINUATION_TOKEN");
                                if (str2 == null) {
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            this.f339e.clear();
            Iterator it3 = arrayList.iterator();
            while (it3.hasNext()) {
                this.f339e.add((C0123a) it3.next());
            }
            this.f340f.clear();
            Iterator it4 = arrayList2.iterator();
            while (it4.hasNext()) {
                this.f340f.add((C0125c) it4.next());
            }
            return true;
        } catch (Exception e) {
            C0078d.m287a(e);
            return false;
        }
    }

    public void close() {
        if (this.f341g != null) {
            try {
                this.f337c.unbindService(this.f342h);
            } catch (Exception unused) {
            }
            this.f341g = null;
        }
    }
}
