package p002eu.chainfire.liveboot;

import android.content.Context;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;

/* renamed from: eu.chainfire.liveboot.c */
public class C0148c {
    /* renamed from: a */
    public static CheckBoxPreference m449a(Context context, PreferenceCategory preferenceCategory, int i, int i2, String str, Object obj) {
        return m450a(context, preferenceCategory, i, i2, str, obj, true);
    }

    /* renamed from: a */
    public static CheckBoxPreference m450a(Context context, PreferenceCategory preferenceCategory, int i, int i2, String str, Object obj, boolean z) {
        CheckBoxPreference checkBoxPreference = new CheckBoxPreference(context);
        if (i > 0) {
            checkBoxPreference.setTitle(i);
        }
        if (i2 > 0) {
            checkBoxPreference.setSummary(i2);
        }
        checkBoxPreference.setEnabled(z);
        checkBoxPreference.setKey(str);
        checkBoxPreference.setDefaultValue(obj);
        if (preferenceCategory != null) {
            preferenceCategory.addPreference(checkBoxPreference);
        }
        return checkBoxPreference;
    }

    /* renamed from: a */
    public static ListPreference m451a(Context context, PreferenceCategory preferenceCategory, int i, int i2, int i3, String str, Object obj, CharSequence[] charSequenceArr, CharSequence[] charSequenceArr2) {
        return m452a(context, preferenceCategory, i, i2, i3, str, obj, charSequenceArr, charSequenceArr2, true);
    }

    /* renamed from: a */
    public static ListPreference m452a(Context context, PreferenceCategory preferenceCategory, int i, int i2, int i3, String str, Object obj, CharSequence[] charSequenceArr, CharSequence[] charSequenceArr2, boolean z) {
        ListPreference listPreference = new ListPreference(context);
        if (i > 0) {
            listPreference.setTitle(i);
        }
        if (i2 > 0) {
            listPreference.setSummary(i2);
        }
        listPreference.setEnabled(z);
        listPreference.setKey(str);
        listPreference.setDefaultValue(obj);
        if (i3 > 0) {
            listPreference.setDialogTitle(i3);
        }
        listPreference.setEntries(charSequenceArr);
        listPreference.setEntryValues(charSequenceArr2);
        if (preferenceCategory != null) {
            preferenceCategory.addPreference(listPreference);
        }
        return listPreference;
    }

    /* renamed from: a */
    public static Preference m453a(Context context, PreferenceCategory preferenceCategory, int i, int i2, boolean z, OnPreferenceClickListener onPreferenceClickListener) {
        Preference preference = new Preference(context);
        if (i > 0) {
            preference.setTitle(i);
        }
        if (i2 > 0) {
            preference.setSummary(i2);
        }
        preference.setEnabled(z);
        if (onPreferenceClickListener != null) {
            preference.setOnPreferenceClickListener(onPreferenceClickListener);
        }
        if (preferenceCategory != null) {
            preferenceCategory.addPreference(preference);
        }
        return preference;
    }

    /* renamed from: a */
    public static PreferenceCategory m454a(Context context, PreferenceScreen preferenceScreen, int i) {
        PreferenceCategory preferenceCategory = new PreferenceCategory(context);
        if (i > 0) {
            preferenceCategory.setTitle(i);
        }
        preferenceScreen.addPreference(preferenceCategory);
        return preferenceCategory;
    }
}
